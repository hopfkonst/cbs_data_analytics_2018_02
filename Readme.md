# Course: "Data Analytics using R"
Instructor: [Konstantin Hopf](mailto:konstantin.hopf@uni-bamberg.de)

Registration: https://www.eventbrite.co.uk/e/data-analytics-workshops-x4-using-r-tickets-42669350175

# Description

Data analytics is a key competency often expected from information systems graduates in companies and academia. This workshop focusses on how the free statistical programming environment R can be used for data analytics. Students will work on concrete and industry-related questions with multiple real-world datasets (e.g., from energy utilities, the finance industry, or online-marketing) and will learn some basics of descriptive statistics, create visualizations and get an idea of machine learning and data science.

# Outline of the course

**Module 1 - Mon, 12/02/2018**: Conceptual overview to business analytics and recap descriptive statistics (CRISP-DM, big data, data types, data quality, classes of analytical problems and a recap of descriptive statistics). *Location: KILEN - Ks43*

**Module 2 - Thurs, 15/02/2018**: Data analytics using R (Overview to R language, data management, working with time and date, applied exercises with R). R studio (free/open source) needed. Location: *HOWITZVEJ - HOW601*

**Module 3 - Mon, 19/02/2018**: Descriptive analytics and visualization (Students learn to prepare a detailed report with descriptive statistics, illustrations among a practical dataset). R studio (free/open source) needed. *Location: KILEN - Ks54*

**Module 4 - Thurs, 22/02/2018**: Machine learning and predictive analytics (Students will learn to build a prediction model based on a real dataset). R studio (free/open source) needed. *Location: HOWITZVEJ - HOW601*

# Contents of this repository and published materials

The slides and all documents regarding the course will be published in this repository during the course. Participants of the course may use the materials for their own and only for education/teaching purpose. All rights on the materials remain by the author, unless otherwise highlighted. Reuse of material may be requested by the author.

# Additional resources and reading materials

The following readings / materials may support this course:

* Introduction to R from [DataCamp](https://www.datacamp.com/courses/free-introduction-to-r)
* [RStudio Webinar](https://www.rstudio.com/resources/webinars/rstudio-essentials-webinar-series-part-1)
* Han, J., Kamber, M., and Pei, J. 2012. Data Mining: Concepts and Techniques, (3. edoition), The Morgan Kaufmann Series in Data Management Systems, Amsterdam: Elsevier.
* Hastie, T., Tibshirani, R., and Friedman, J. 2009. The Elements of Statistical Learning, Springer Series in Statistics, New York, NY: Springer New York.
* IBM (2013), "Descriptive, predictive, prescriptive: Transforming asset and facilities management with analytics" [Link](https://static.ibmserviceengage.com/TIW14162USEN.PDF)
* Müller, O., Junglas, I., Brocke, J. vom and Debortoli, S. (2016), "Utilizing big data analytics for information systems research: challenges, promises and guidelines", European Journal of Information Systems, Vol. 25 No. 4, pp. 289-302.



